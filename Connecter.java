import sun.security.krb5.Config;

import java.io.*;
import java.net.*;

/**
 * Connecter class is used to connect 
 * hosts according to the parameter passed
 * that stablished connection mode
 */

public class Connecter  
{
    private ConnectionCreator creator;

    //parameter constuctor
    public Connecter()
    {}

    /* @param connection It is a Connection interface obj
     * declaration connecter obj. It allow to connect
     * on a specific Connection interface obj, and to
     * get it input or output stream.
     * @return Stream This is a Stream interface obj
     * that contains input and output flow.
     */
    public Stream connectTo(Connection connection)
    {
        if (connection instanceof SocketObj)
        {
            try
            {
                this.creator = new ConnectionCreator (connection);
            }
            catch (IOException e)
            {
                e.printStackTrace ();
            }

            return creator.getStream("Tcp");
        }

        else return null;
    }
}   