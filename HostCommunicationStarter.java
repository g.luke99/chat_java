import java.io.*;
import java.net.*;
/*
* This class implements the Client or Server passage to connect, receive or send data
* according to the parameters passed in the constructor.
* Every Clients should be create an obj of this class and passed
* hostname and portnumber, while every
* Servers should be create an obj of this class and passed
* only portnumber. This implementation allows to
* send or receive data through Socket, with an P2P architecture*/

public class HostCommunicationStarter
{
    private Socket socket;
    private Log lg = new Log("HostCommunicationStarter.java");
    private Input input = null;
    private SocketSender socketSender = null;
    private Stream stream = null;
    private Data data = null;

    /*
    * @param hostname this is the host name of the client
    * @param portnumber this is the port number of the client
     */
    //parameters constructor
    public HostCommunicationStarter(String hostName, int portNumber)
    {
        socket = SocketCreator.createClientSocket (hostName, portNumber);
    }

    /*
     * @param portnumber this is the port number of the server
     */
    //parameters constructor
    public HostCommunicationStarter(int portNumber)
    {
        socket = SocketCreator.createServerSocket (portNumber);
    }


    /**
     * This method starts communication, it called manageSend
     * and manageReceive method.
     */
    public void startCommunication()
    {
        startCreatingConnection (socket);
        manageReceive ();
        manageSend ();
    }

    /*
    *This method create connection and it set the Stream interface obj
    * to be used by manageSend() method to send data.
    * @param s this is implementation of Connecter method connectTo().
     */
    private void startCreatingConnection (Socket s)
    {
        /*declaration connecter obj. It allow to connect
        * on a specific Connection interface obj, and to
        * get it input or output stream.
         */
        Connecter connecter = new Connecter();
        this.stream = connecter.connectTo (new SocketObj (s));
        System.out.println ("Online");
        lg.insert ("HostCommunicationStarter Online");
    }


    /*
     * This method create the data according to the dataType passed
     * @param dataType this is the data type to send on other host
     * @return Data this return is the data to send on other host
     */

    private Data manageData()
    {
        return input.manageInput ();
    }

    private void manageReceive()
    {
        SocketReceiver r = new SocketReceiver(stream.getInputStream());
        Thread t = new Thread(r);
        t.start ();
    }

    /*
    * This method manage the send creating the SocketSender obj
    * and to send data call the manageData () method that create
    * data.*/
    private void manageSend()
    {
        this.socketSender = new SocketSender (stream.getOutputStream ());
        input = new Input (socketSender);

        while(true)
        {
            //call the manageData method to create data
            this.data = manageData();

            try
            {
                //send data to SocketSender
                socketSender.send(data);
            }

            catch(IOException e)
            {
                System.out.println("Error: ");
                e.printStackTrace();
                lg.insert("Error");
                System.exit(1);
            }
        }
    }
}
