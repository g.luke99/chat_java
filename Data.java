/**
 * this is interface define the Data obj type.
 * It's used to create a Data obj to send it.
 */
public interface Data
{
    int getHeader();
    String getPayload();
    String getTime();
    String getSender();
    String getReceiver();
}