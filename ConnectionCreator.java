import java.io.*;
import java.net.*;

/**
 * StreamFactory create the stream according to the type passed as parameter 
 */

public class ConnectionCreator
{
    private TcpConnection connection;
    private BufferedReader in;
    private PrintWriter out;

    //parameter constructor
    public ConnectionCreator(Connection connection) throws IOException
    {
        if(connection instanceof SocketObj)
        {
            createSocketStreams((Socket) connection.getConnectionObj ());
        }
    }

    /**
     * @param s
     * @throws IOException
     */
    private void createSocketStreams (Socket s) throws IOException
    {
        this.in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        this.out = new PrintWriter(s.getOutputStream());
    }

    /*this method return the stream according to the parameter passed
    * @param streamType This is the stream type parameter*/
    public Stream getStream(String streamType)
    {
        if (streamType.equals("Tcp")) 
        {
            return connection = new TcpConnection(in, out);
        }
        else return null;
    }
}