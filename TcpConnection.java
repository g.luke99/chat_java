import java.io.*;
/**
 * This class implememts Stream interface
 * define the Tcp stream type.it contains
 * input and output flow.
 */
public class TcpConnection implements Stream
{
    private BufferedReader in;
    private PrintWriter out;

    /*
    * This is parameters constructor
    * @param in this is the BufferedReader parameter for input
    * @param out this is the PrintWriter parameter for output*/
    public TcpConnection(BufferedReader in, PrintWriter out)
    {
        this.in  = in;
        this.out = out;
    }

    /*@return BufferedReader this is the input stream*/
    //this method return input stream
    public BufferedReader getInputStream()
    {
        return this.in;
    }

    /*@return PrintWriter this is the input stream*/
    //this method return output stream
    public PrintWriter getOutputStream() 
    {
        return this.out;    
    }
}