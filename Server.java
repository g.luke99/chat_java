import java.io.*;
import java.net.*;

/**
 * Server. this is the main class
 */
public class Server 
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        //take socket parameters by args
        if(args.length != 1)
        {
            System.out.println("Type: java Server <portNumber>");
            System.exit(1);
        }

        //assigned parameters to variables
        int portNumber = Integer.parseInt(args[0]);
        //HostCommunicationStarter Declaration to start application
        HostCommunicationStarter hostCommunicationStarter = new HostCommunicationStarter (portNumber);
        hostCommunicationStarter.startCommunication ();
    }    
}