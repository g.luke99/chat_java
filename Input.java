/*This class manage the RawConsoleInput input, the deleting,
* and the case that the str String is null.*/

import java.io.*;

public class Input
{
    private boolean isTyping = false;
    private DataFactory dataFactory = new DataFactory ();
    private SocketSender socketSender;

    /**
     * @param socketSender This is the Socketsendere obj to send the data
     */
    public Input(SocketSender socketSender)
    {
        this.socketSender = socketSender;
    }


    /**
     * @return Data It may be any Data interface obj according to the implementation
     */
    public Data manageInput()
    {
        int ch;
        StringBuilder str = new StringBuilder ();

        while(true)
        {
            try
            {
                boolean stop = false;
                ch = RawConsoleInput.read(true);
                //if char is endLine
                if (ch == 13)
                {
                    int i = str.length ();
                    if(i > 0)
                    {
                        System.out.print ("\n");
                        isTyping = false;
                        Data d = dataFactory.getData (Protocol.MESSAGE, str.toString ());
                        str = new StringBuilder ();
                        return d;
                    }

                    else{
                        stop = true;
                    }
                }
                //if char is backspace
                if(ch == 8)
                {
                    if(str.length () == 0)
                    {continue;}

                    int lenght = str.length ();
                    str = new StringBuilder (str.substring (0, str.length () - 1));
                    System.out.print ("\r" + String.format("%" + lenght + "s", ""));
                    System.out.print ("\r" + str);

                    if(str.length () == 0)
                    {
                        isTyping = false;
                        socketSender.send(dataFactory.getData (Protocol.NOTICE, " "));
                    }
                }
                //if char is a letter
                else if(ch <= 255)
                {
                    if(!stop)
                    {
                        System.out.print((char)ch);
                        str.append ((char) ch);

                        if(!isTyping)
                        {
                            isTyping = true;
                            socketSender.send(dataFactory.getData (Protocol.NOTICE, "is typing..."));
                        }
                    }
                }
                //if str is logout
                if(str.toString ().equals("logout"))
                    {System.exit(1);}
                //System.out.print("\r" + str);
            }

            catch (IOException e)
            {
                System.out.println("error");
                System.exit(1);
            }
        }
    }
}