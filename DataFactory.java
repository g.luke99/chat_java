/*this class create Datas that will be sended
by Sender class.*/

import java.time.LocalTime;

public class DataFactory
{
    private LocalTime localTime;
    public DataFactory()
    {}

    /**
     * @param dataType This is the message dataType
     * @param message This is the message
     * @return Data This is the data created according to the dataType
     */
    //this method returns the data according to the parameter passed
    public Data getData(int dataType, String message)
    {
        localTime = localTime.now();
        String time = localTime.getHour () + ":" + localTime.getMinute ();

        //these if compare the dataType parameter with the Protocol constant
        if(dataType == Protocol.MESSAGE)
        {
            return new Message(message, "Message", time);
        }

        else if(dataType == Protocol.NOTICE)
        {
            return new Notice(message, "", "");
        }
 
        else return null;
    }
}
