import java.io.*;
import java.net.*;

/**
 * Client. this is main class
 */
public class Client 
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        //take socket parameters by args
        if(args.length != 2)
        {
            System.out.println("Type: java Client <Host name> <portNumber>");
            System.exit(1);
        }
 
        //assigned parameters to variables
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        //HostCommunicationStarter Declaration to start application
        HostCommunicationStarter hostCommunicationStarter = new HostCommunicationStarter (hostName, portNumber);
        hostCommunicationStarter.startCommunication ();
    }    
}