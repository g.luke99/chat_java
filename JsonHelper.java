import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author
 * Convert Object to Json and Json  to Message Object
 *
 */
public class JsonHelper
{
    //json mapper obj
    private static ObjectMapper mapper = new ObjectMapper();

    /**
     * @param data
     * @return String
     * Convert Message Object in to JSON String
     */
    public static String toJson(Data data) throws JsonProcessingException
    {
        String json = "";
        return json = mapper.writeValueAsString(data);
    }

    /**
     * @param jsonString
     * @return Message
     * Convert
     */

    public static Data toObject(String jsonString, int dataType) throws IOException
    {
        Data data = null;
        if (dataType == Protocol.MESSAGE)
        {
            data = (Message) mapper.readValue (jsonString, Message.class);
        }

        else if (dataType == Protocol.NOTICE)
        {
            data = (Notice) mapper.readValue (jsonString, Notice.class);
        }

        return data;
    }
}
