import java.io.*;
import java.net.*;

/*This class create the socket, according to the
parameter constructor type, to the server e to the 
client.*/

public class SocketCreator
{
    /**
     * This method create Socket to Client
     * @param hostName This is the Client host name
     * @param portNumber This is the Client port number
     * @return Socket This is the Socket
     */
    //static method
    public static Socket createClientSocket(String hostName, int portNumber)
    {
        Socket socket = null;

        try
        {
            socket = new Socket(hostName, portNumber);
        }

        catch(IOException e)
        {
            System.out.println("Error: ");
            e.printStackTrace();
           // lg.insert(logError);
            System.exit(1);
        }

        return socket;
    }

    /**
     * This method create Socket to Server
     * @param portNumber This is the Server port number
     * @return Socket This is the Socket
     */
    //create a Server Socket
    public static Socket createServerSocket(int portNumber)
    {
        ServerSocket serverSocket;
        Socket socket = null;

        try
        {
            serverSocket = new ServerSocket(portNumber);
            socket = serverSocket.accept();
        }

        catch(IOException e)
        {
            System.out.println("Error: ");
            e.printStackTrace();
            //log insert
            //lg.insert(logError);
            System.exit(1);
        }
        return socket;
    }
}