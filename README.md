# Project Title

Chat 3.0

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
1. Windows 7 or higher
2. JAVAC compiler for Ubuntu
3. jdk1.8.0_151 
```

### Installing

```
1. Open Windows CMD
2. Digit this command: set "PATH=%PATH%;C:\Program Files\Java\jdk1.8.0_151\bin"
```

### Testing

To compile the sources 

```
javac -cp jackson-annotations-2.6.3.jar;jackson-core-2.6.3.jar;jackson-databind-2.6.3.jar;jna-3.5.2; *.java
```

##To run application 

Run Server

```
java -cp jackson-annotations-2.6.3.jar;jackson-core-2.6.3.jar;jackson-databind-2.6.3.jar;jna-3.5.2;. Server [port number]
```
Run Client

```
java -cp jackson-annotations-2.6.3.jar;jackson-core-2.6.3.jar;jackson-databind-2.6.3.jar;jna-3.5.2;. Client [Host Name][port number]
```

## Authors

* **Luca Gagliardo** - *Initial work* 

## Limitations

This application allows to send or receive message simultaneously. To comply with specifics it should be run in a Command Prompt and not in any IDE.

