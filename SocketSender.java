import java.io.*;
/**
 * SocketSender is used to send an Data obj.
 * It contains the send() method to send a String.
 */
public class SocketSender 
{
    //private attribute
    private PrintWriter out;
    ProcessHandler handler = new OnlyJsonProcess ();

    /*Constructor parameter
    * @param out This is the output PrintWriter parameter
    * to send data on other host BufferedReader*/
    public SocketSender(PrintWriter out)
    {
        this.out = out;
    }

    /*Send method to send a data
    * @param data This is the data to send on other host
    */
    public void send(Data data) throws IOException
    {
        //send data
        out.println(handler.handleOut (data));
        out.flush();
    }
}