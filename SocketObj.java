import java.net.*;

public class SocketObj implements Connection<Socket>
{
    private Socket socket;

    public SocketObj(Socket socket)
    {
        this.socket = socket;
    }

    @Override
    public Socket getConnectionObj()
    {
        return this.socket;
    }

    @Override
    public void setConnectionObj(Socket socket)
    {
        this.socket = socket;
    }
}
