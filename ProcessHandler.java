public interface ProcessHandler
{
    String handleOut(Data t);
    Data handleIn(String s);
}
