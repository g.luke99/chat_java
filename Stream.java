import java.io.*;

/**
 * this is interface define the connection stream type
 */

interface Stream 
{
    BufferedReader getInputStream();
    PrintWriter getOutputStream();
}