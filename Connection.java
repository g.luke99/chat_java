/**
 * this is interface define the Connection obj type.
 * It's used to create a Connection obj to pass as
 * parameter in Connecter class.
 */

public interface Connection<T>
{
    T getConnectionObj();
    void setConnectionObj(T t);
}
