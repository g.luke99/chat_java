/*this class write log in a log file.
it is many generale purpose to allow
to write all log file types*/

import java.io.*;
import java.util.*;

public class Log 
{
	private String classe;
	private File doc = new File("C:/Users/luke/Desktop/azienda/chat 3.0/log.txt");
	private GregorianCalendar gc = new GregorianCalendar();
	
	//constructor that assigned the 'classe' attribute 
	public Log(String classe)
	{
		this.classe = classe;
	}
	
	//metod that write in a file log
	public void insert(String action)
	{
		//declaration stream reference
		FileWriter fw = null;
		PrintWriter pw = null;
		
		try
		{
			fw = new FileWriter(doc, true);
			pw = new PrintWriter(fw);
		}
		
		catch(IOException e)
		{
			System.out.println("Errore apertura file" + e.getMessage());
			System.exit(1);
		}
		
			//write in file
			pw.println("LOG" + ":" + classe + " " + action + ":" + gc.get(Calendar.DAY_OF_YEAR) + ":" + gc.get(Calendar.MONTH) + gc.get(Calendar.YEAR) + ":" + gc.get(Calendar.HOUR) + ":" + gc.get(Calendar.MINUTE) + ":" + gc.get(Calendar.SECOND));
			pw.flush();
			pw.close();

	}
}		