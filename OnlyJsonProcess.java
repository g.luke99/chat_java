import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.IOException;

public class OnlyJsonProcess implements ProcessHandler
{

    /**
     * @param t This is the data to convert in json
     * @return String this is the converted string json
     */
    @Override
    public String handleOut(Data t)
    {
        String json = "";

        try
        {
            if(t instanceof Message)
            {
                json = Protocol.applyProtocol (JsonHelper.toJson (t), Protocol.MESSAGE);
            }

            else if(t instanceof Notice)
            {
                json = Protocol.applyProtocol (JsonHelper.toJson (t), Protocol.NOTICE);
            }
        }

        catch (IOException e)
        {
            e.printStackTrace ();
            e.getMessage ();
            System.exit (1); ;
        }

        return json;
    }

    /**
     * @param json This is the json to convert in obj
     * @return Data This is the converted obj by json
     */
    @Override
    public Data handleIn(String json)
    {
        Data data = null;
        int type = Protocol.analyzedProtocol (json);
        try
        {
            if (type == Protocol.MESSAGE)
            {
                Protocol.removeProtocol (json);
                data = JsonHelper.toObject (json, Protocol.MESSAGE);
            }

            else if (type == Protocol.NOTICE)
            {
                Protocol.removeProtocol (json);
                data = JsonHelper.toObject (json, Protocol.NOTICE);
            }
        }

        catch (JsonParseException e)
        {
            e.printStackTrace ();
            e.getMessage ();
            System.exit (1);
        } catch (JsonMappingException e) {
            e.printStackTrace ();
        } catch (IOException e) {
            e.printStackTrace ();
        }

        return data;
    }
}

