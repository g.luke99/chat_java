/*this class define Message data type.
* It contains header, payload, sender,
* receiver, time attributes, that offer
* information of the Message data*/


public class Message implements Data
{
    //private attributes
    private int header;
    private String payload;
    private String sender;
    private String receiver;
    private String time;

    /**
     * Public parameter constructor
     * @param payload This is the Message payload
     * @param sender This is the Message sender
     * @param time This is the creation time data
     */
    //parameters constructor
    public Message(String payload, String sender, String time)
    {
        this.header  = Protocol.MESSAGE;
        this.payload = payload;
        this.sender = sender;
        this.time = time;
    }
    //dafault constructor
    public Message()
    {}
    
    @Override
    public int getHeader()
    {
        return this.header;    
    }

    @Override
    public String getPayload()
    {
        return this.payload;
    }

    @Override
    public String getSender()
    {
        return this.sender;
    }

    @Override
    public String getReceiver()
    {
        return this.receiver;
    }

    @Override
    public String getTime()
    {
        return this.time;
    }
}
