/*This class manage the receiving of Data.
* It implements runnable interface to receiving
* data, by other host, in indipendent way.*/

import java.io.*;

public class SocketReceiver implements Runnable
{
  //private attribute
  private BufferedReader receive;
  private ProcessHandler handler = new OnlyJsonProcess ();
  private String str = null;
  private Data data = null;

  /*Constructor with an Read Stream like parameter
  * @param receive This is the input BuffereReader parameter
  * to read data by its buffer.*/

  public SocketReceiver(BufferedReader receive)
  {
    this.receive = receive;
  }

    /**
     * This is the implementation of Runnable interface
     */
  @Override
  public void run()
  {
    while(true)
    {
        try
        {
          //#reading step
          str = receive.readLine();
          //#conversion step
          data = handler.handleIn (str);
          //#printing data content in output
          System.out.println(data.getTime () + " | " + data.getSender() + ": " + data.getPayload ());
        }

        catch (IOException e)
        {
          e.printStackTrace ();
          System.exit(1);
        }
    }
  }
}
